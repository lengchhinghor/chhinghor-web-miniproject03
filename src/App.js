import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { useEffect } from 'react';
import { changeLanguage } from './services/localization.service';
import { Container } from 'react-bootstrap';
import NavMenu from './components/NavMenu';
import Author from './pages/Author';
import Home from './pages/Home';
import ViewArticleDetail from './view/ViewArticleDetail'
import CreateArticle from './view/CreateArticle';
import Category from './pages/Category';


function App() {
	useEffect(() => {
		changeLanguage("kh")
		}, [changeLanguage]);
	 
	return (
		<Router>
			<NavMenu />
			<Container>
				<Switch>
					<Route exact path='/' component={Home} />
					<Route path='/article' component={CreateArticle} />
					<Route path='/article/update/:articleId' component={CreateArticle} />
					<Route path='/detail/:id' component={ViewArticleDetail} />
					<Route path='/author' component={Author} />
					<Route path='/category' component={Category} />
				</Switch>
			</Container>
		</Router>
	);
}

export default App;
