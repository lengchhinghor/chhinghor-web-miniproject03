import React, { useEffect,  } from 'react';
import { Container, Table,} from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchCategory} from '../redux/actions/categoryAction';
import { strings } from "../localization/localization";

function Category() {

	const category = useSelector((state) => state.category.category);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(onFetchCategory());
	}, []);

	return (
		<Container>
			<h1 className='my-3'>{strings.category}</h1>
			

			<Table striped bordered hover>
				<thead>
					<tr>
						<th>#</th>
						<th>{strings.name}</th>
					</tr>
				</thead>
				<tbody>
					{category.map((categories, index) => (
						<tr key={categories._id}>
							<td>{index + 1}</td>
							<td>{categories.name}</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
}

export default Category;
