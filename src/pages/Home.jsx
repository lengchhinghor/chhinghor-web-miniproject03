import React, { useEffect, useState } from 'react';
import { Container, Table, Button, Col, Row, Form, Card } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchArticles, onDeleteArticle } from '../redux/actions/articleAction';
import { onFetchCategory} from '../redux/actions/categoryAction';
import { uploadImage } from '../services/authorService';
import { useHistory } from "react-router-dom";
import { strings } from "../localization/localization";



function Home() {

	const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png');
	const articles = useSelector((state) => state.articles.articles);
	const category = useSelector((state) => state.category.category);
	const dispatch = useDispatch();
	const history = useHistory();
	const [selectedId, setSelectedId] = useState('');

	useEffect(() => {
		dispatch(onFetchArticles());
		dispatch(onFetchCategory());
	}, []);

	const resetForm=()=>{
		console.log("clicked")
	  }

	const onDelete = (id) => {
		if (id === selectedId) resetForm();

		dispatch(onDeleteArticle(id));
	};
	
	console.log(category)
	return (
			<Row className="mt-5">
				<Col md={3}>
				<Card style={{ width: '18rem' }}>
				<Card.Img variant="top" src="image/avatar.jpg" />
				<Card.Body>
					<Card.Title>{strings.plese_log}</Card.Title>
					<Button variant="primary">{strings.gowhere}</Button>
				</Card.Body>
				</Card>
				</Col>
				<Col md={9}>
				<h2>{strings.category}</h2>
			{category.map((category) => (
				<Button size='sm' variant='secondary' className='mx-1 mt-2 my-4'>{category.name}</Button>
			))}
				<Row>
				{ articles.map((article)=> (
				<Col md={3} key={article._id}>
					<Card className="my-2">
				<Card.Img
				variant="top"
				style={{ objectFit: "cover", height: "150px" }}
				src={article.image ? article.image : 'https://designshack.net/wp-content/uploads/placeholder-image.png '} />
				<Card.Body>
					<Card.Title>{article.title}</Card.Title>
					<Card.Text>
					{article.description}		
					</Card.Text>
					<Button size="sm" variant="primary" onClick={()=>history.push(`/detail/${article._id}`)}>
					{strings.read}
			     	</Button>{" "}
					 <Button size="sm" variant="warning" onClick={()=>history.push(`/article/update/${article._id}`)}>
					 {strings.edit}
					</Button>{" "}
					<Button size='sm' variant='danger' onClick={() => onDelete(article._id)}>
					{strings.delete}
					</Button>
				</Card.Body>
			</Card>
				</Col>
				
			))
			}
				</Row>
				</Col>
			</Row>
			
	);
}

export default Home;
