import { fetchArticles, postArticle, deleteArticle, updateArticleById, fetchArticleById } from '../../services/articleService.js';
import {articleActionType} from './articleActionType';

export const onFetchArticles = () => async (dispatch) => {
	let articles = await fetchArticles();
	dispatch({
		type: articleActionType.FETCH_ARTICLES,
		payload: articles,
	});
};

export const onFetcArticleById = (articleId) => async (dispatch) => {
	let article = await fetchArticleById(articleId);
	dispatch({
		type: articleActionType.FETCH_ARTICLEBYID,
		payload: article,
	})
}

export const onInsertArticle = (newArticle) => async (dispatch) => {
	let article = await postArticle(newArticle);

	dispatch({
		type: articleActionType.INSERT_ARTICLE,
		payload: article,
	});
};
	
export const onDeleteArticle = (articleId) => async (dispatch) => {
	let message = await deleteArticle(articleId);

	dispatch({
		type: articleActionType.DELETE_ARTICLE,
		payload: articleId,
	});
};

export const onUpdateArticle = (articleId, updatedArticle) => async (dispatch) => {
	let message = await updateArticleById(articleId, updatedArticle);

	dispatch({
		type: articleActionType.UPDATE_ARTICLE,
		payload: { articleId, updatedArticle },
	});
};
