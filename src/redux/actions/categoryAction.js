import { fetchCategory} from '../../services/categoryService';
import {categoryActionType} from './categoryActionType';

export const onFetchCategory = () => async (dispatch) => {
	let category = await fetchCategory();
	dispatch({
		type: categoryActionType.FETCH_CATEGORIES,
		payload: category,
	});
};
