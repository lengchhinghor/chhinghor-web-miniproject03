import { categoryActionType } from '../actions/categoryActionType';

const initialState = {
	category: [],
};

const categoryReducer = (state = initialState, action) => {
	switch (action.type) {
		case categoryActionType.FETCH_CATEGORIES:
			return { ...state, category: [...action.payload] };
		default:
			return state;
	}
	
};

export default categoryReducer;
