import { authorActionType } from '../actions/authorActionType';

const initialState = {
	authors: [],
};

const authorReducer = (state = initialState, action) => {
	switch (action.type) {
		case authorActionType.FETCH_AUTHORS:
			return { ...state, authors: [...action.payload] };
		case authorActionType.INSERT_AUTHOR:
			return { ...state, authors: [action.payload, ...state.authors] };

		case authorActionType.DELETE_AUTHOR:
			return { ...state, authors: state.authors.filter((author) => author._id !== action.payload) };

		case authorActionType.UPDATE_AUTHOR:
			console.log(action);
			let newAuthors = [...state.authors];
			newAuthors = newAuthors.map((author) => {
				if (author._id === action.payload.authorId) {
					author.name = action.payload.updatedAuthor.name;
					author.email = action.payload.updatedAuthor.email;
					author.image = action.payload.updatedAuthor.image;
				}
				return author;
			});

			return { ...state, authors: newAuthors };

		default:
			return state;
	}
};

export default authorReducer;
