import { combineReducers } from 'redux';
import authorReducer from './authorReducer';
import articleReducer from './articleReducer';
import categoryReducer from './categoryReducer';
const rootReducer = combineReducers({
    authors: authorReducer,
    articles: articleReducer,
    category: categoryReducer
});

export default rootReducer;
