import React from 'react';
import { Navbar, Container, Nav, Form, FormControl, Button, NavDropdown  } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import { strings } from "../localization/localization";
import { changeLanguage } from "../services/localization.service";
import { useEffect } from 'react';
function NavMenu() {
	useEffect(() => {
		changeLanguage("en")
		}, []);
	return (
		<Navbar bg='primary' expand='lg'>
			<Container>
				<Navbar.Brand  className="text-light" as={NavLink} to='/'>
					KSHRD-Chhinghor
				</Navbar.Brand>
				<Navbar.Toggle aria-controls='basic-navbar-nav' />
				<Navbar.Collapse id='basic-navbar-nav' >
					<Nav className='me-auto text-light'>
						<Nav.Link as={Link} to='/' className="text-light">
						{strings.home}
						</Nav.Link>
						<Nav.Link as={NavLink} to='/article' className="text-light">
						{strings.article}
						</Nav.Link>
						<Nav.Link as={NavLink} to='/author' className="text-light">
						{strings.author}
						</Nav.Link>
						
						<Nav.Link as={NavLink} to='/category' className="text-light">
						{strings.category}
						</Nav.Link>
						<Nav>
							<NavDropdown  title="Languages" id="basic-nav-dropdown" >
							<NavDropdown.Item onClick={()=>changeLanguage('kh')}>
								Khmer
							</NavDropdown.Item>
							<NavDropdown.Item onClick={()=>changeLanguage('en')}>
								English
							</NavDropdown.Item>
							</NavDropdown>
						</Nav>
					</Nav>

					<Form className='d-flex'>
						<FormControl type='search' placeholder='Search' className='me-2' aria-label='Search' />
						<Button variant='outline-success'>Search</Button>
					</Form>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}

export default NavMenu;
