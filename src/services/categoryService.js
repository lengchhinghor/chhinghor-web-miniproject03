import api from '../utils/apis';

export const fetchCategory = async () => {
	let response = await api.get('/category');
	return response.data.data;
};
