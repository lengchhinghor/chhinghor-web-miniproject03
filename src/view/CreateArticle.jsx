import React, { useEffect, useState } from 'react';
import { Container, Table, Button, Col, Row, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchAuthors} from '../redux/actions/authorAction';
import { onInsertArticle, onUpdateArticle, onFetcArticleById } from '../redux/actions/articleAction';
import { uploadImage } from '../services/authorService';
import { useHistory, useParams} from "react-router-dom";
import { strings } from "../localization/localization";

function CreateArticle() {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [getAuthor, setGetAuthor] = useState({});
	const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png');
	const [imageFile, setImageFile] = useState(null);
	const [selectedId, setSelectedId] = useState('');
	const [selectedAuthor, setselectedAuthor] = useState({});
	const dispatch = useDispatch();
	const history = useHistory();
	let { articleId } = useParams();
	const onResetForm=()=>{
		console.log("clicked")
		setTitle("");
		setDescription("");
		setImageFile("")
		setImageURL("https://designshack.net/wp-content/uploads/placeholder-image.png")
		history.push(`/article`)
	  }
	  const authors = useSelector((state)=>state.authors.authors)
	  
		useEffect(()=>{
			dispatch(onFetchAuthors());
		},[])
		let onAddOrUpdate = async (e) => {
			e.preventDefault();
	
			let newArticle = {
				title,
				description,
			};
	
			if (imageFile) {
				let url = await uploadImage(imageFile);
				newArticle.image = url;
			} else {
				newArticle.image = imageURL;
			}
	
			if (selectedId) {
				dispatch(onUpdateArticle(selectedId, newArticle));
			} else {
				dispatch(onInsertArticle(newArticle));
			}
	
			onResetForm();
		};

		console.log(articleId)
		useEffect(() => {
			console.log(articleId)
			if (articleId) {
			onFetcArticleById(articleId).then((e) => {
				if (e) {
				console.log(e, "fetch");
				setTitle(e?.title);
				setDescription(e?.description);
				setImageURL(e?.image || null);
				}
			}).catch((err)=>onResetForm());
			}
		}, []);

	return (
		<Container>
			<h1 className='my-3'>{strings.article}</h1>
			<Row>
				<Col md={8}>
					<Form>
						<Form.Group controlId='title'>
							<Form.Label>{strings.name}</Form.Label>
							<Form.Control
								type='text'
								placeholder='Author Name'
								value={title}
								onChange={(e) => setTitle(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>
						<Form.Group className="mb-3 text-left" controlId="exampleForm.ControlSelect1">
						<Form.Label>{strings.author}</Form.Label>
						<Form.Control as="select">
							{authors.map((author)=>(
							<option key={author._id} onClick={()=>selectedAuthor(author)}>{author.name}</option>
							))}
						</Form.Control>
						</Form.Group>
						<Form.Group controlId='description'>
							<Form.Label>{strings.description}</Form.Label>
							<Form.Control
								type='text'
								placeholder='Description'
								value={description}
								onChange={(e) => setDescription(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Button variant='primary' onClick={onAddOrUpdate}>
							{selectedId ? 'Update' : 'Add'}
						</Button>
					</Form>
				</Col>
				<Col md={4}>
					<img className='w-100' src={imageURL} />
					<Form>
						<Form.Group>
							<Form.File
								id='img'
								label='Choose Image'
								onChange={(e) => {
									let url = URL.createObjectURL(e.target.files[0]);
									setImageFile(e.target.files[0]);
									setImageURL(url);
								}}
							/>
						</Form.Group>
					</Form>
				</Col>
			</Row>
		</Container>
	);
}

export default CreateArticle;
