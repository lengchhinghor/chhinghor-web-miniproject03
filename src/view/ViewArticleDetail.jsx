import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Col, Container, Row,Card } from "react-bootstrap";
import { useParams} from 'react-router'
import { onFetcArticleById } from '../redux/actions/articleAction'; 

 function ViewArticleDetail() {
    let  { id } = useParams()
    const dispatch = useDispatch();
    const articles = useSelector((state) => state.articles.articles);
    useEffect(()=>{
        dispatch(onFetcArticleById(id))
    }, [])
    
    console.log(articles)
    return (
        <Container>
            <Row >
                <Col md={8}> 
                {
                     articles.map((article)=>(
                        <Col className="my-2" key={article._id}>
                        <Card className="p-2">
                        <Card.Title className="text-left">{article.title}</Card.Title>
                        <Card.Img
                            style={{ objectFit: "cover"}}
                            className={article.image ? "img-fluid" : "d-none img-fluid"}
                            variant="top"
                            src={article.image ? article.image : "https://blackmantkd.com/wp-content/uploads/2017/04/default-image-620x600.jpg"}
                        />
                        <Card.Body>
                            <Card.Title>{article.description}</Card.Title>
                        </Card.Body>
                        </Card>
                    </Col>

                    ))
                }
                </Col>
               
            </Row>
        </Container>
    )
}

export default ViewArticleDetail;
